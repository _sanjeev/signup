import React from "react";
import validator from "validator";
import "./form.css";

class Form extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      age: "",
      password: "",
      confirmpassword: "",
      checkbox: false,
      nameError: "",
      emailError: "",
      ageError: "",
      passwordError: "",
      confirmpasswordError: "",
      checkedError: false,
      status: false,
    };
  }

  handleChange = (event) => {
    let value = event.target.value;

    if (event.target.name === "checkbox") {
      value = event.target.checked;
    }

    this.setState({
      [event.target.name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    let errorMsg = {
      nameError: "",
      emailError: "",
      ageError: "",
      passwordError: "",
      confirmpasswordError: "",
      checkedError: "",
      status: true,
    };

    if (validator.isEmpty(this.state.name)) {
      errorMsg.status = false;
      errorMsg.nameError = "Please Enter your name";
    }

    if (!validator.isEmpty(this.state.name)) {
      const name = this.state.name.trimStart(" ");
      const noOfSpaces = this.state.name.split(" ").join("");
      if (this.state.name.length !== name.length) {
        errorMsg.status = false;
        errorMsg.nameError = "Please enter a valid name";
      } else if (!validator.isAlpha(noOfSpaces, "en-GB")) {
        errorMsg.status = false;
        errorMsg.nameError = "Name can contains [a-z, A-Z, space]";
      }
    }

    if (validator.isEmpty(this.state.email)) {
      errorMsg.status = false;
      errorMsg.emailError = "Please Enter an email";
    }

    if (
      validator.isEmpty(this.state.email) === false &&
      validator.isEmail(this.state.email, {
        blacklisted_chars: "~ ` ! @ # $ % ^ & * ( ) - + =",
      }) === false
    ) {
      errorMsg.status = false;
      errorMsg.emailError = "Please Enter a valid email";
    }

    if (validator.isEmpty(this.state.age)) {
      errorMsg.status = false;
      errorMsg.ageError = "Please Enter an age";
    }

    if (
      validator.isEmpty(this.state.age) === false &&
      (this.state.age <= 17 || this.state.age > 120)
    ) {
      errorMsg.status = false;
      errorMsg.ageError = "Enter a valid age from 18 to 120";
    }

    if (validator.isEmpty(this.state.password)) {
      errorMsg.status = false;
      errorMsg.passwordError = "Please Enter your password";
    }

    if (
      validator.isEmpty(this.state.password) === false &&
      validator.isStrongPassword(this.state.password, {
        minLength: 6,
        minNumbers: 1,
        minUppercase: 1,
        minSymbols: 1,
      }) === false
    ) {
      errorMsg.status = false;
      errorMsg.passwordError =
        "Password length at least 6 character uppaercase: 1, numbers: 1, symbol: 1";
    }

    if (validator.isEmpty(this.state.confirmpassword)) {
      errorMsg.status = false;
      errorMsg.confirmpasswordError = "Please Enter your confirm password";
    }

    if (
      validator.isEmpty(this.state.confirmpassword) === false &&
      this.state.password !== this.state.confirmpassword
    ) {
      errorMsg.status = false;
      errorMsg.confirmpasswordError = "Password didnot match";
    }

    if (this.state.checkbox === false) {
      errorMsg.status = false;
      errorMsg.checkedError = "Agree the terms and conditions";
    }

    this.setState(errorMsg);
  };

  render() {
    return (
      <div>
        <div>
          {this.state.status === false ? (
            <div className=" w-75 shadow-lg mb-5 bg-white rounded mx-auto mt-4 overflow-auto overflow-hidden d-flex">
              <div className="container w-55 d-flex mx-auto">
                <form
                  className="row g-3 mw-70"
                  onSubmit={this.handleSubmit}
                  className="form w-70 pt-4 pb-4 mx-auto"
                >
                  <div className="col-md-4 input-div w-100 pt-4">
                    <div>
                      {" "}
                      <label htmlFor="name" className=" ">
                        {" "}
                        Name
                      </label>
                    </div>
                    <div>
                      {" "}
                      <input
                        type="text"
                        className="w-85 form-control"
                        id="name"
                        name="name"
                        onChange={this.handleChange}
                        placeholder="Enter your name"
                      />
                    </div>

                    <div className="">
                      <div className="text-danger">{this.state.nameError}</div>
                    </div>
                  </div>

                  <div className="col-md-4 w-100 pt-4">
                    <div>
                      {" "}
                      <label htmlFor="email" className="">
                        Email
                      </label>
                    </div>
                    <div>
                      <input
                        type="text"
                        className=" w-85 form-control"
                        id="email"
                        name="email"
                        onChange={this.handleChange}
                        placeholder="Enter your email"
                      />
                    </div>

                    <div className="">
                      <div className="text-danger">{this.state.emailError}</div>
                    </div>
                  </div>

                  <div className="col-md-4 w-100 pt-4">
                    <div>
                      <label htmlFor="age" className="form-label">
                        Age
                      </label>
                    </div>
                    <div>
                      {" "}
                      <input
                        type="number"
                        className="w-85 form-control"
                        id="age"
                        name="age"
                        onChange={this.handleChange}
                        placeholder="Enter your age"
                      />
                    </div>

                    <div>
                      <div className="text-danger">{this.state.ageError}</div>
                    </div>
                  </div>

                  <div className="col-md-4 w-100 pt-4">
                    <div>
                      <label htmlFor="password" className="form-label">
                        Password
                      </label>
                    </div>
                    <div>
                      {" "}
                      <input
                        type="password"
                        className=" w-85 form-control"
                        id="password"
                        name="password"
                        onChange={this.handleChange}
                        placeholder="Enter your password"
                      />
                    </div>

                    <div className="">
                      <div className="text-danger">
                        {this.state.passwordError}
                      </div>
                    </div>
                  </div>

                  <div className="col-md-4 w-100 pt-4">
                    <div>
                      <label htmlFor="confirmPassword" className="form-label">
                        Confirm Password
                      </label>
                    </div>
                    <div>
                      {" "}
                      <input
                        type="password"
                        className=" w-85 form-control"
                        id="confirmPassword"
                        name="confirmpassword"
                        onChange={this.handleChange}
                        placeholder="Enter confirm password"
                      />
                    </div>

                    <div className="">
                      <div className="text-danger">
                        {this.state.confirmpasswordError}
                      </div>
                    </div>
                  </div>

                  <div className="col-12 w-100 pt-4 d-flex-col mx-auto my-auto">
                    <div className="d-flex-col">
                      <div className="checkbox d-flex justify-content-center align-items-center mx-auto py-auto">
                        <input
                          type="checkbox"
                          value=""
                          id="checkbox"
                          name="checkbox"
                          onChange={this.handleChange}
                        />
                        <label className="px-1" htmlFor="checkbox">
                          Agree terms and conditions
                        </label>
                      </div>
                      <div className="">
                        <div className="text-danger">
                          {this.state.checkedError}
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="col-12 button input-button d-flex mx-auto">
                    <button
                      className="mx-auto bg-primary text-white w-50 py-2 border-0"
                      type="submit"
                    >
                      Register
                    </button>
                  </div>
                </form>
              </div>
              <div className="bg-primary text-white w-100 d-flex align-items-center d-none d-sm-block">
                <div className="h-100 d-flex justify-content-center align-items-center">
                  <h1 className="text-center">SignUp Form</h1>
                </div>
              </div>
            </div>
          ) : (
            <div className="submitted mt-4">
              <h1 className="text-center mt-5">Form Submitted Successfully</h1>
              <div className="text-center">Name: {this.state.name}</div>
              <div className="text-center">Email: {this.state.email}</div>
              <div className="text-center">Age:{this.state.age}</div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Form;
