import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Nav extends Component {
    render() {
        return (
            <div className='container-fluid d-flex justify-content-center bg-primary'>
                <Link to='/' className='m-3 text-white text-decoration-none'>
                    Home
                </Link>
                <Link to='/about' className='m-3 text-white text-decoration-none'>
                    About
                </Link>
                <Link to='/signup' className='m-3 text-white text-decoration-none'>
                    Signup
                </Link>
            </div>
        );
    }
}

export default Nav;
