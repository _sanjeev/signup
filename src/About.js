import React, { Component } from "react";

class About extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <h1 className="text-center mt-3">About</h1>
        <div className="d-flex justify-content-center">
          <p className="w-75 text-center text-secondary border border-dark">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus,
            voluptatum necessitatibus accusantium perferendis sequi, modi omnis
            totam eius itaque ut minima aliquam, unde nihil nobis dolores
            eligendi reprehenderit minus ipsam dicta impedit quod blanditiis
            rerum iure! Assumenda tempora quasi facilis provident ipsa neque
            ipsum, sit culpa incidunt, explicabo ab nihil! Voluptatum incidunt
            eaque nisi rem, soluta nam deleniti officiis cupiditate hic. Dolorem
            minus repellendus aliquam. Quasi, sit nobis beatae asperiores sint
            quas a tempore excepturi doloremque eveniet quae pariatur modi,
            voluptatem repellendus accusamus animi perferendis omnis
            consequuntur qui neque nihil. Incidunt pariatur fugit deserunt
            sapiente id nulla, in sint distinctio dolorum corporis ut molestias.
            Asperiores quaerat, saepe temporibus inventore tempora eaque?
            Voluptatem perspiciatis commodi repellat. Aperiam aliquid odit fugit
            deserunt eaque recusandae alias adipisci nam neque excepturi dolores
            nesciunt dolorum dignissimos incidunt, optio aliquam eligendi iste!
            Rerum autem praesentium doloremque. Molestiae, ex. Illum nisi
            reiciendis dicta placeat eaque, vel ullam rerum fuga adipisci
            exercitationem ad quas beatae repudiandae minus alias nihil soluta.
            A eius placeat maiores? Inventore numquam dolore dolores magnam
            rerum quisquam voluptatibus ullam illum dolorem nesciunt culpa
            delectus laborum suscipit perferendis nam, nulla optio architecto
            qui odit quod amet enim nihil non. Magni recusandae quisquam atque
            suscipit autem.
          </p>
        </div>
      </div>
    );
  }
}

export default About;
