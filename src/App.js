import React, { Component } from 'react';
import Form from './Form';
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Nav from './Nav';

class App extends Component {
  render() {
    return (
      <Router>
      <Nav />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/about' component={About} />
          <Route exact path='/signup' component={Form} />
        </Switch>
      </Router>
    );
  }
}

export default App;